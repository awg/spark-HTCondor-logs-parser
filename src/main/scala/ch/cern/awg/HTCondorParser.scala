package ch.cern.awg

import java.util.concurrent.Executors
import scala.Array.canBuildFrom
import scala.concurrent.Await
import scala.concurrent.ExecutionContext
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import scala.concurrent.duration.Duration
import scala.concurrent.duration.DurationInt
import scala.util.Failure
import scala.util.Success
import org.apache.hadoop.fs.FileSystem
import org.apache.hadoop.fs.Path
import org.apache.log4j.Logger
import org.apache.spark.SparkConf
import org.apache.spark.SparkContext
import org.apache.spark.sql.SaveMode
import org.apache.spark.sql.hive.HiveContext
import org.apache.spark.storage.StorageLevel
import com.google.common.util.concurrent.ThreadFactoryBuilder
import org.apache.spark.sql.functions.coalesce
import org.apache.spark.sql.DataFrame

object HTCondorParser {

  private[this] lazy val _sqlContext = {
    {
      val conf = new SparkConf()
        .set("spark.scheduler.mode", "FAIR")
        .set("spark.sql.caseSensitive", "true")
        .set("spark.executor.cores", "2")
        .set("spark.executor.memory", "2G")
      val sc = new SparkContext(conf)
      val sqlc = new HiveContext(sc)
      (sc, sqlc)
    }
  }

  private[this] var numberOfThreads: Option[Int] = None

  private[this] lazy val threadPool = {
    {
      val threadFactory =
        new ThreadFactoryBuilder()
          .setDaemon(true)
          .setNameFormat("spark-pool-%s")
          .build()
      if (numberOfThreads.isDefined){
        println("=> Number of threads: numberOfThreads.get")
        ExecutionContext.fromExecutor(Executors.newFixedThreadPool(numberOfThreads.get, threadFactory))
      }
      else
        ExecutionContext.fromExecutor(Executors.newCachedThreadPool(threadFactory))
    }
  }

  def stopSpark() {
    val future = Future(_sqlContext._1.stop)(threadPool)
    Await.result(future, 60.seconds)
  }

  def JSON2Parquet(date: String, inputDayPath: String, outputDayPath: String,
    entriesPerFile: Long, logger: Logger): Future[String] = {

    Future {

      logger.info(s"=> Processing: input=$inputDayPath, output=$outputDayPath")

      val (sc, hiveCtx) = _sqlContext

      val sqlc = hiveCtx.newSession()
      import sqlc.implicits._

      val dfJson = sqlc.read.json(inputDayPath)
      dfJson.persist(StorageLevel.MEMORY_AND_DISK)

      val count = dfJson.count()
      logger.info(s"=> date: $date, path $inputDayPath, total: $count")

      sc.setLocalProperty("spark.scheduler.pool", "write")

      val columns = dfJson.columns

      var resultDF: DataFrame = sqlc.emptyDataFrame
      
      if (columns.contains("Remote_JobUniverse") && columns.contains("remote_jobuniverse")) {
        // it is necessary to coalesce columns with equal names but with different case size
        resultDF = dfJson.withColumn("Remote_JobUniverse_merged", coalesce($"Remote_JobUniverse", $"remote_jobuniverse"))
          .drop($"Remote_JobUniverse")
          .drop($"remote_jobuniverse")
          .withColumnRenamed("Remote_JobUniverse_merged", "Remote_JobUniverse")
      } else {
        resultDF = dfJson
          .withColumnRenamed("remote_jobuniverse", "Remote_JobUniverse")
      }
      
      resultDF
        .repartition(if (count / entriesPerFile != 0) Math.ceil(count / entriesPerFile).toInt else 1)
        .write
        .mode(SaveMode.Overwrite)
        .parquet(outputDayPath)

      resultDF.unpersist()

      s"=> input=$inputDayPath, output=$outputDayPath, count=$count"
    }(threadPool)
  }

  def main(args: Array[String]) {

    var logger = Logger.getLogger(HTCondorParser.this.getClass())

    if (args.length < 3) {
      logger.error("=> wrong number of parameters")
      System.err.println("Usage: HTCondorParser <0:raw input path> <1:output path> " +
        "<2: entriesPerFile> <3: numThread (Optional)>")
      System.exit(1)
    }

    if (args.length == 4)
      HTCondorParser.this.numberOfThreads = Some(args(3).toInt)

    val future = Future(_sqlContext)(threadPool)
    Await.result(future, 1200.second)
    val (sc, sqlc) = future.value.get.get

    val rawJsonPath = args(0)
    val outputPath = args(1)
    val entriesPerFile = args(2).toLong

    val fs = FileSystem.get(sc.hadoopConfiguration)

    logger.info(s"=> rawJsonPath $rawJsonPath")
    logger.info(s"=> outputPath $outputPath")
    logger.info(s"=> entriesPerFile $entriesPerFile")

    val yearsList = fs.listStatus(new Path(rawJsonPath)).filter(f => f.isDir())
    val monthsList = fs.listStatus(yearsList.map(_.getPath)).filter(f => f.isDir())
    val daysList = fs.listStatus(monthsList.map(_.getPath)).filter(f => f.isDir())

    val daysToProcess = daysList.map(fstatus => (fstatus.getPath.toString.split("/").toList.takeRight(3), fstatus))
      .map { case (date, fstatus) => (date(0), date(1), date(2), fstatus) }
      .filter { case (year, month, day, fstatus) => !day.contains(".tmp") && year.matches("20[0-9][0-9]") }
      .map { case (year, month, day, fstatus) => (year, month, day, fstatus, s"$outputPath/year=${year.toInt}/month=${month.toInt}/day=${day.toInt}") }
      .filter {
        case (year, month, day, fstatus, outputDayPath) =>
          {
            val parquetPath = new Path(outputDayPath)
            if (fs.exists(parquetPath))
              if (fs.getFileStatus(parquetPath).getModificationTime < fstatus.getModificationTime)
                true
              else {
                if (!(fs.exists(new Path(outputDayPath + "/_SUCCESS"))))
                  true
                else
                  false
              }
            else
              true
          }
      }

    // if (daysToProcess.length > 10)
    //   if (daysToProcess.length > 50)
    //     sc.requestExecutors(50)
    // if (HTCondorParser.this.numberOfThreads.isEmpty)
    //   HTCondorParser.this.numberOfThreads = Some(25)
    // else
    //   sc.requestExecutors(10)
    // if (HTCondorParser.this.numberOfThreads.isEmpty)
    //   HTCondorParser.this.numberOfThreads = Some(5)
    // else if (daysToProcess.length > 2)
    //   sc.requestExecutors(4)
    // if (HTCondorParser.this.numberOfThreads.isEmpty)
    //   HTCondorParser.this.numberOfThreads = Some(2)

    if (daysToProcess.length > 50){
      sc.requestExecutors(50)
      if (HTCondorParser.this.numberOfThreads.isEmpty)
        HTCondorParser.this.numberOfThreads = Some(25)
    } else if (daysToProcess.length > 10){
      sc.requestExecutors(10)
      if (HTCondorParser.this.numberOfThreads.isEmpty)
        HTCondorParser.this.numberOfThreads = Some(5)
    } else {
      sc.requestExecutors(4)
      if (HTCondorParser.this.numberOfThreads.isEmpty)
        HTCondorParser.this.numberOfThreads = Some(2)
    }

    var futureResults = new Array[Future[String]](daysToProcess.length)

    logger.info(s"=> Using ${HTCondorParser.this.numberOfThreads.get} threads.")
    logger.info(s"=> Processing ${daysToProcess.length} days.")

    val futures = for (i <- 0 to daysToProcess.length - 1) yield {
      val date = s"${daysToProcess(i)._1}${daysToProcess(i)._2}${daysToProcess(i)._3}"
      val inputDayPath = daysToProcess(i)._4.getPath.toString
      val outputDayPath = daysToProcess(i)._5
      JSON2Parquet(date, inputDayPath, outputDayPath, entriesPerFile, logger)
    }

    logger.info(s"=> Awaiting for completion.")

    val f = Future.sequence(futures.toList)

    Await.result(f, Duration.Inf)

    logger.info(s"=> Parallel execution completed.")

    f onComplete {
      case Success(messages) => for (message <- messages) logger.info(message)
      case Failure(t) => logger.error("An error has occured: " + t.getMessage + " " + t.toString())
    }

    stopSpark()

  }
}
