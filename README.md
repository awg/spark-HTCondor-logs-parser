# Spark HTCondor logs parser

This project processes daily folders of condor batch accounting logs and creates the same folders structure in Parquet format.

## Build command

The command used to build the project

```bash
git clone https://:@gitlab.cern.ch:8443/awg/spark-HTCondor-logs-parser.git
cd spark-HTCondor-logs-parser
mvn clean package -DskipTests
```

Artifacts can be found in the ``./target/`` folder

## Run command

Assuming data are stored in a Hadoop cluster and that the application will run in cluster mode:

```bash
spark-submit \
  --master yarn-client \
  --files fairscheduler.xml \
  --conf spark.scheduler.allocation.file=fairscheduler.xml \
  spark-HTCondor-logs-parser-0.1-SNAPSHOT-jar-with-dependencies.jar \
    /user/accservice/batch/condor/ \
    batch-condor \
    10000000
```

Arguments:

1)  ``/user/accservice/batch/condor/`` is the input path

2) ``batch-condor`` is the output path

3) ``10000000`` is the desired number of entries per file and it's used to tune the number of output files (for each daily folder). This should be changed, considering the input folder size instead.

It is required that input data are stored with path format ``./YYYY/MM/DD/*.json`` and output will be ``./year=YYYY/month=m/day=d/*.parquet``.

# CI Build status [![build status](https://gitlab.cern.ch/awg/spark-HTCondor-logs-parser/badges/master/build.svg)](https://gitlab.cern.ch/awg/spark-HTCondor-logs-parser/commits/master)

Automatically built artifacts can be found

* in the EOS public folder:
	* available at the path ``/eos/user/a/awgdpl/spark-HTCondor-parser``
	* or from the web page: https://cernbox.cern.ch/index.php/s/M65cRlShwGEcsKE
* in the pipelines web page: https://gitlab.cern.ch/awg/spark-HTCondor-logs-parser/pipelines
  * by default gitlab deletes artifact older than
* in docker images: https://gitlab.cern.ch/awg/spark-HTCondor-logs-parser/container_registry


## Run with Docker

Requirements:

* Kerberos ticket
* Hadoop configurations (e.g. to connect to *analytix* Hadoop cluster)
* Docker

Check available versions here: https://gitlab.cern.ch/awg/spark-HTCondor-logs-parser/container_registry

Pull docker image:

```bash
docker pull gitlab-registry.cern.ch/awg/spark-htcondor-logs-parser:v0.1
```

Run with:

```bash
docker run -it --net=host -e KRB5CCNAME=$KRB5CCNAME -e AFS_USER=$USER \
    -v /tmp:/tmp -v /afs:/afs \
    -v /etc/hadoop/conf/:/etc/hadoop/conf \
    gitlab-registry.cern.ch/awg/spark-htcondor-logs-parser:v0.1
```

The container image will run the command displayed above. However if you need to modify some parameter you can overwrite the default command appending a new one, like this:

```bash
docker run -it --net=host -e KRB5CCNAME=$KRB5CCNAME -e AFS_USER=$USER \
    -v /tmp:/tmp -v /afs:/afs \
    -v /etc/hadoop/conf/:/etc/hadoop/conf \
    gitlab-registry.cern.ch/awg/spark-htcondor-logs-parser:v0.1 spark-submit --master yarn-client --files fairscheduler.xml --conf spark.scheduler.allocation.file=fairscheduler.xml spark-HTCondor-logs-parser-0.1-SNAPSHOT-jar-with-dependencies.jar /user/accservice/batch/condor/ batch-condor 10000000
```

If the machine contains the puppetized Hadoop client installation with Spark, you may use the following to inject Spark configuration files (for instance to enable dynamic resource allocation):

```bash
docker run -it --net=host -e KRB5CCNAME=$KRB5CCNAME -e AFS_USER=$USER \
    -v /tmp:/tmp -v /afs:/afs \
    -v /etc/hadoop/conf/:/etc/hadoop/conf -v /etc/spark/conf:/opt/spark/conf \
    gitlab-registry.cern.ch/awg/spark-htcondor-logs-parser:v0.1
```

## acrontab

The following crontab runs against a machine part of the hostgroup ``awg/hadoop_siam/jobsubmit`` (you can find the code [here](https://gitlab.cern.ch/ai/it-puppet-hostgroup-awg/blob/master/code/manifests/hadoop_siam/jobsubmit.pp)).

```
00 5 * * * awg-jobsubmit.cern.ch docker pull gitlab-registry.cern.ch/awg/spark-htcondor-logs-parser:v0.1; docker run -i --net=host -e KRB5CCNAME=$KRB5CCNAME -e AFS_USER=$USER -v /tmp:/tmp -v /afs:/afs -v /etc/hadoop/conf/:/etc/hadoop/conf gitlab-registry.cern.ch/awg/spark-htcondor-logs-parser:v0.1
```
